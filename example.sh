#!/bin/sh
#***********************************************************************************************************************
#*
#* NorthernWind Site Archetypes
#* Copyright (C) 2012-2023 by Tidalwave s.a.s. (http://www.tidalwave.it)
#*
#***********************************************************************************************************************
#*
#* Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
#* the License. You may obtain a copy of the License at
#*
#*     http://www.apache.org/licenses/LICENSE-2.0
#*
#* Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
#* an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
#* specific language governing permissions and limitations under the License.
#*
#***********************************************************************************************************************
#*
#***********************************************************************************************************************
#

set -x

rm -rf acme-mynewproject

mvn archetype:generate -B \
    -DarchetypeGroupId=it.tidalwave.northernwind \
    -DarchetypeArtifactId=simple-project-site-archetype-1 \
    -DsiteName=MyNewProject \
    -DsiteUrl=acme/mynewproject \
    -Dauthor="Willy Coyote" \
    -DgroupId=acme.mynewproject \
    -DartifactId=acme-mynewproject \
    -Dversion=1.11 \
    -DcopyrightHolder="Acme inc." \
    -DaddthisPubid=willycoyote \
    -DgoogleSiteVerification=thecode \
    -DgoogleAnalyticsId=UA-XXXX \
    -DohlohId=mynewproject

